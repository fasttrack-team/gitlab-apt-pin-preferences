# gitlab-apt-pin-preferences 

Creates apt pinning preferences file for gitlab for a smoother dependency resolution.

### Steps to add new preferences

- Edit the `99gitlab` file. Use following template for new preferences:
```
    Package: <PACKAGE_NAME>
    Pin: release n=<BRANCH_NAME>
    Pin-Priority: <PRIORITY>
```
- Make a changelog entry in `debian/changelog`.
